## Book Sessions app

React + TS app that allow users to book pairing sessions.

- Custom Data is a plain JSON list
- User can navigate to individual session page
  to review the session description.
- User is able to book/schedule a learning session.
- User is able to see all booked sessions in the navigation bar
- User is able to cancel a booked session on the same modal
  tha renders the list of booked session.

### Technical Stack

- React
- Typescript
- Shared state for booked session
  provided via React Context.
- Vite
