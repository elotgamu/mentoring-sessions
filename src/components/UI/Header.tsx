import { NavLink } from "react-router-dom";

import Button from "../Button";
import { useState } from "react";
import Modal from "../Modal";
import UpcomingSessions from "../Sessions/UpcomingSessions";

/**
 *
 * Custom Header component for
 * app navigation between internal pages
 *
 */
export default function Header() {
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  function onShowUpcomingSessions() {
    setModalVisible(true);
  }
  function handleModalClose() {
    setModalVisible(false);
  }
  return (
    <>
      {modalVisible && (
        <Modal onClose={handleModalClose}>
          <UpcomingSessions />
          <div className="actions">
            <Button onClick={handleModalClose}>Cancel</Button>
          </div>
        </Modal>
      )}
      <header id="main-header">
        <h1>React Mentoring</h1>

        <nav>
          <ul>
            <li>
              <NavLink to="/" title="Our Mission">
                Our Mission
              </NavLink>
            </li>
            <li>
              <NavLink to="/sessions" title="Browse Sessions">
                Browse Sessions
              </NavLink>
            </li>
            <li>
              <Button type="button" onClick={onShowUpcomingSessions}>
                Upcoming Sessions
              </Button>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
}
