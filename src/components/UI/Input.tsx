import { ComponentPropsWithoutRef } from "react";

type InputProps = ComponentPropsWithoutRef<"input"> & {
  name: string;
  label: string;
  id: string;
};

export default function Input({ label, ...props }: InputProps) {
  return (
    <div className="control">
      <label htmlFor={props.id}>{label}:</label>
      <input {...props} />
    </div>
  );
}
