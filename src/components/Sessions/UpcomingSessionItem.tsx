import { useBookSession, type BookedSession } from "../../context/BookSession";
import Button from "../Button";

// component props
type UpcomingSessionItemProps = {
  session: BookedSession;
};

/**
 *
 * Upcoming session item
 *
 * Render individual booked session
 * and the ability to cancel the booked
 * session
 *
 */
export default function UpcomingSessionItem({
  session,
}: UpcomingSessionItemProps) {
  const { cancelSession } = useBookSession();

  function handleCancelBookedSession() {
    cancelSession(session.id);
  }

  return (
    <article className="upcoming-session">
      <div>
        <h3>{session.title}</h3>
        <p>{session.summary}</p>
        <time dateTime={new Date(session.bookDate).toISOString()}>
          {new Date(session.bookDate).toLocaleDateString("en-US", {
            day: "numeric",
            month: "short",
            year: "numeric",
          })}
        </time>
      </div>
      <p className="actions">
        <Button type="button" textOnly onClick={handleCancelBookedSession}>
          Cancel
        </Button>
      </p>
    </article>
  );
}
