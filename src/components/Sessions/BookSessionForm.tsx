import { useRef, type FormEvent } from "react";
import Button from "../Button";
import Input from "../UI/Input";

type BookSessionProps = {
  onSubmit: (data: { name: string; email: string }) => void;
  onCancel: () => void;
};
function BookSessionForm({ onCancel, onSubmit }: BookSessionProps) {
  const formRef = useRef<HTMLFormElement>(null);

  function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const rawData = new FormData(event.currentTarget);
    const data = Object.fromEntries(rawData) as { name: string; email: string };
    onSubmit(data);
  }
  return (
    <div>
      <h2>Book Session</h2>
      <form ref={formRef} onSubmit={handleSubmit}>
        <Input
          id="session-book-name"
          name="name"
          label="Your Name"
          type="text"
          required
        />
        <Input
          id="session-book-email"
          name="email"
          label="Email"
          type="email"
          required
        />

        <div className="actions">
          <Button type="button" onClick={onCancel} textOnly>
            Cancel
          </Button>
          <Button type="submit">Book Session</Button>
        </div>
      </form>
    </div>
  );
}

export default BookSessionForm;
