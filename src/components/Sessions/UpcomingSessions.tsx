import { useBookSession } from "../../context/BookSession";
import UpcomingSessionItem from "./UpcomingSessionItem";

export default function UpcomingSessions({}) {
  const { sessions } = useBookSession();
  return (
    <div>
      <h2>Upcoming Sessions</h2>

      {!sessions.length ? <p>No sessions booked</p> : null}

      <ul>
        {sessions.map((session) => (
          <li key={session.id}>
            <UpcomingSessionItem session={session} />
          </li>
        ))}
      </ul>
    </div>
  );
}
