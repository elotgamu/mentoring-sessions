import { ComponentPropsWithoutRef } from "react";

import { Link } from "react-router-dom";

type LinkTypeProps = ComponentPropsWithoutRef<"a"> & {
  to: string;
  textOnly?: boolean;
};

type ButtonTypeProps = ComponentPropsWithoutRef<"button"> & {
  textOnly?: boolean;
  to?: never;
};

/**
 * We make use of "type predicates"
 * to help TS understand what "type"
 * of boolean we're returning
 * this is basically an user-defined
 * type guard.
 * See the return type defined with
 * the "is" keyword to ensure that
 * `to` is in props so the prop param
 * is of type LinkTypeProps
 * */

function isLinkProps(
  props: LinkTypeProps | ButtonTypeProps
): props is LinkTypeProps {
  return "to" in props;
}

/**
 *
 * Custom Button component that renders
 * a Link element if the `to` prop is
 * present. Otherwise it renders a button tag
 *
 * The name Button derives from the fact that
 * from a styling perspective the returned content
 * has button style like, unless we pass the
 * `textOnly` boolean prop which visually
 * transform the output into a outline style
 * button
 */
export default function Button(props: LinkTypeProps | ButtonTypeProps) {
  if (isLinkProps(props)) {
    const { textOnly, children, ...otherProps } = props;
    const linkClasses = `button ${textOnly ? "button--text-only" : ""}`;
    return (
      <Link className={linkClasses} {...otherProps}>
        {children}
      </Link>
    );
  }

  // this somehow repetitive destructuring
  // is needed to help TS understand that
  // at this point the props is now a ButtonTypeProps
  // Also we do not want to pass children
  // nor textOnly props to the button itself
  // because those are not valid standard attributes
  // accepted by a button tag
  const { children, textOnly, ...buttonProps } = props;
  const buttonClasses = `button ${textOnly ? "button--text-only" : ""}`;
  return (
    <button className={buttonClasses} {...buttonProps}>
      {children}
    </button>
  );
}
