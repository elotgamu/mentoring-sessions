import { createPortal } from "react-dom";
import {
  forwardRef,
  useImperativeHandle,
  type ComponentPropsWithoutRef,
  useRef,
  useEffect,
} from "react";

// props for the exposed methods
export type ModalHandlerProps = {
  openModal: () => void;
};

// Props for the modal for now only children
// is needed
type ModalProps = ComponentPropsWithoutRef<"dialog"> & {
  onClose: () => void;
};

/**
 * Custom Modal component that exposes an
 * `openModal` method for parent component
 * to invoke it and open the modal.
 *
 * Because this modal is conditionally
 * rendered by parent state, we have to
 * include a useEffect to trigger the
 * show dialog native method
 * But the effect can be on a wrapper component
 * that renders this modal component
 *
 * Note: We do not need the exposed
 * `openModal` method because the modal
 * is conditionally rendered by the parent
 * component who invokes it however we keep
 * it for the purpose of this project
 *
 */
const Modal = forwardRef<ModalHandlerProps, ModalProps>(function Modal(
  { children, onClose },
  ref
) {
  const modalRef = useRef<HTMLDialogElement>(null);

  // expose a openModal method to trigger
  // the modal to display
  useImperativeHandle(ref, () => {
    return {
      openModal() {
        if (modalRef.current) {
          modalRef.current?.showModal();
        }
      },
    };
  });

  // This allow us to open the modal
  // once it gets the mounted status
  // a wrapper component can implement this
  useEffect(() => {
    if (modalRef.current) {
      modalRef.current.showModal();
    }
  }, []);

  return createPortal(
    <dialog ref={modalRef} className="modal" onClose={onClose}>
      {children}
    </dialog>,
    document.getElementById("modal-root")!
  );
});

export default Modal;
