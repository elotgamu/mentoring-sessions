import Button from "./Button";

type SessionProps = {
  session: {
    id: string;
    image: string;
    title: string;
    summary: string;
  };
};
export default function Session({ session }: SessionProps) {
  return (
    <article className="session-item">
      <img src={session.image} alt="" />
      <div className="session-data">
        <h3>{session.title}</h3>
        <p>{session.summary}</p>
        <div className="actions">
          <Button type="button" to={`/sessions/${session.id}`}>
            Learn More
          </Button>
        </div>
      </div>
    </article>
  );
}
