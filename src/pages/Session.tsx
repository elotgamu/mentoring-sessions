import { useParams } from "react-router-dom";

import Modal, { ModalHandlerProps } from "../components/Modal.tsx";
import Button from "../components/Button.tsx";

// data
import { SESSIONS } from "../dummy-sessions.ts";
import { useRef, useState } from "react";
import BookSessionForm from "../components/Sessions/BookSessionForm.tsx";
import { useBookSession } from "../context/BookSession.tsx";

export default function SessionPage() {
  const [showBookModal, setShowBookModal] = useState<boolean>(false);
  const modalRef = useRef<ModalHandlerProps>(null);
  const params = useParams<{ id: string }>();

  const { bookSession } = useBookSession();

  const sessionId = params.id;
  const loadedSession = SESSIONS.find((session) => session.id === sessionId);

  if (!loadedSession) {
    return (
      <main id="session-page">
        <p>No session found!</p>
      </main>
    );
  }

  function handleBookSession() {
    setShowBookModal(true);
    modalRef.current?.openModal();
  }

  function onHideBookModal() {
    setShowBookModal(false);
  }

  function onSubmitBooking(data: { name: string; email: string }) {
    if (data && loadedSession) {
      bookSession({
        id: loadedSession.id,
        title: loadedSession.title,
        summary: loadedSession.summary,
        duration: loadedSession.duration,
      });
      setShowBookModal(false);
    }
  }

  return (
    <main id="session-page">
      {showBookModal && (
        <Modal ref={modalRef} onClose={onHideBookModal}>
          <BookSessionForm
            onCancel={onHideBookModal}
            onSubmit={onSubmitBooking}
          />
        </Modal>
      )}
      <article>
        <header>
          <img src={loadedSession.image} alt={loadedSession.title} />
          <div>
            <h2>{loadedSession.title}</h2>
            <time dateTime={new Date(loadedSession.date).toISOString()}>
              {new Date(loadedSession.date).toLocaleDateString("en-US", {
                day: "numeric",
                month: "short",
                year: "numeric",
              })}
            </time>
            <p>
              {/*
               * // TODO:
               * Add logic to open a modal to capture user data
               * to book a session
               */}
              <Button type="button" onClick={() => handleBookSession()}>
                Book Session
              </Button>
            </p>
          </div>
        </header>
        <p id="content">{loadedSession.description}</p>
      </article>
    </main>
  );
}
