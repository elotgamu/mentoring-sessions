import { Outlet } from "react-router-dom";
import Header from "../components/UI/Header";
import BookSessionContextProvider from "../context/BookSession";

export default function Root() {
  return (
    <BookSessionContextProvider>
      {/* Todo: Add Header */}
      <Header />
      <Outlet />
    </BookSessionContextProvider>
  );
}
