import Session from "../components/Session.tsx";

// The data:
// normally, we would probably load that from a server
import { SESSIONS } from "../dummy-sessions.ts";

export default function SessionsPage() {
  return (
    <main id="sessions-page">
      <header>
        <h2>Available mentoring sessions</h2>
        <p>
          From an one-on-one introduction to React's basics all the way up to a
          deep dive into state mechanics - we got just the right session for
          you!
        </p>
      </header>

      <ul id="sessions-list">
        {SESSIONS.map((session) => (
          <li key={session.id}>
            <Session session={session} />
          </li>
        ))}
      </ul>
    </main>
  );
}
