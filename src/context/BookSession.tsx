import { type ReactNode, createContext, useReducer, useContext } from "react";

// Base Session Data
type Session = {
  id: string;
  title: string;
  summary: string;
  duration: number;
};

// A booked session is basically a Session with a bookDate
export type BookedSession = Session & { bookDate: number };

// The book Session Data
type BookedSessionContext = {
  sessions: BookedSession[];
};

// the session action handlers
type BookedSessionContextHandler = {
  bookSession: (session: Session) => void;
  cancelSession: (id: string) => void;
};

// the context provider props
type BookedSessionContextProviderProps = {
  children: ReactNode;
};

type BookSessionAction = {
  type: "BOOK_SESSION";
  payload: Session;
};

type CancelBookedSession = {
  type: "CANCEL_BOOKED_SESSION";
  payload: string;
};

type BookSessionActionTypes = BookSessionAction | CancelBookedSession;

// the context type for this session
// where we have the data plus the handler to expose
type BookSessionContextType = BookedSessionContext &
  BookedSessionContextHandler;

const initialData: BookedSessionContext = {
  sessions: [],
};
const BookSessionContext = createContext<BookSessionContextType | null>(null);

function bookSessionReducer(
  state = initialData,
  action: BookSessionActionTypes
): BookedSessionContext {
  switch (action.type) {
    case "BOOK_SESSION":
      const newSession = { ...action.payload, bookDate: new Date().getTime() };
      return { ...state, sessions: [...state.sessions, newSession] };

    case "CANCEL_BOOKED_SESSION":
      const sessionId = action.payload;
      const filteredSession = state.sessions.filter(
        (session) => session.id !== sessionId
      );
      return { ...state, sessions: filteredSession };

    default:
      return state;
  }
}

function BookSessionContextProvider({
  children,
}: BookedSessionContextProviderProps) {
  const [booking, dispatch] = useReducer(bookSessionReducer, initialData);

  const contextData: BookSessionContextType = {
    sessions: booking.sessions,
    bookSession(data: Session) {
      dispatch({ type: "BOOK_SESSION", payload: data });
    },
    cancelSession(id: string) {
      dispatch({ type: "CANCEL_BOOKED_SESSION", payload: id });
    },
  };
  return (
    <BookSessionContext.Provider value={contextData}>
      {children}
    </BookSessionContext.Provider>
  );
}

export function useBookSession() {
  const context = useContext(BookSessionContext);

  if (!context) {
    throw new Error(
      "BookSession Context has to be used within a Context Provider"
    );
  }

  return context;
}

export default BookSessionContextProvider;
